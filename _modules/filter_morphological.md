---
title:     Morphological Filters
layout:    module
prerequisites:
  - "[Neighbourhood filters](filter_neighbourhood)"
  - "[Rank filters](filter_rank)"
objectives:
  - "Design morphological filters using rank filters"
  - "Execute morphological filters on binary or grayscale images and explain the output"
motivation: >
  Filters can be used to change size and shape of objects in an image. [*] Concept map below assumes bright objects on dark background. For dark objects on bright background effect of min and max filters is inverted.
concept_map: >
  graph TD
      image --> max1[max]
      image --> min1[min]
      image --> max2[max]
      image --> min2[min]
      image --> d
  subgraph rank filter sequence
      max2 --> min3[min]
      min2 --> max3[max]
      max1
      min1
      d[max - min]
      end
      max1 --> dilation
      min1 --> erosion
      max3 --> opening
      min3 --> closing
      d --> gradient
      subgraph morphological filter name
      dilation
      erosion
      opening
      closing
      gradient
      end

# figure: TODO
# figure_legend: TODO

activity_preface: >
  Open the image, `xy_8bit_binary_two_spots_different_size.tif`, and explore how structures grow and shrink, using erosion and dilation

# activities:
#   "Platform": "path/to/file.md"

exercises_preface: >

  ### Exercise 1

  Fill in the blanks, using those words: "shrinks", "increases", "decreases", "enlarges".

  1. An erosion _____ objects in a binary image.
  2. An erosion in a binary image _____ the number of foreground pixels.
  3. A dilation in a grayscale image _____ the average intensity in the image.
  4. A dilation _____ objects in a binary image.


  ### Exercise 2

  True of false? Discuss with your neighbour!

  1. Morphological openings on binary images can decrease the number of foreground pixels.
  2. Morphological closings on binary images never decreases the number of foreground pixels.
  3. Performing a morphological closing a twice in a row does not make sense, because the second closing does not further change the image.

# exercises:
#   "Platform": "path/to/file.md"

learn_next:
  - TODO

external_links:
  - "[Wikipedia: Morphological Gradient](https://en.wikipedia.org/wiki/Morphological_gradient)"
  - "[ImageJ docs: Greyscale morphological filters](https://imagej.net/MorphoLibJ#Grayscale_morphological_filters)"
---

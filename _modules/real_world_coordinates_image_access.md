```mermaid
graph TD
    VV(Voxel values) --> |accessed by| VI(Voxel indices)
    VV(Voxel values) --> |accessed by| RWC(Real world coordinates)
```
